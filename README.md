# CarCar

Team:

* Jose Lopez - Sales
* Andrew Gaston - Services

## Design

TEST VINs = 1GYFK63878R248711, JH4DA1850KS002731

## Service microservice
Models:
class Technician - Name, EmployeeID
class ServiceAppointment - VIN, Owner, TimeScheduled(date/time)
class AutomobileVO -  VIN, Href


Views:
Employee_list - PUT, DELETE, POST, GET
   This listview of the employees will use the Tehcnician Model's encoder to populate the Technician form for the Service Department

Service_appt_list - POST, GET
   This list view will show a list of all

Service_appt_detail - POST, GET, PUT, DELETE


The models are setup in a manner that will allow our AutomobileVO to be our linking key between the Sales department's and Service department's microservices; this is accomplished using each instance of "Automobile"'s unique identifier: VIN. Using the AutomobileVOEncoder, the Services API is able to access all of the information from the pre-existing models in the InventoryAPI. This is because the poller looks for an instance of the VIN in all of objects in the database, and then populates the ServiceAPI's forms and database with the requisite fields' properties. For the purposes of populating the data fields of the service list and the VO(vin)


## Sales microservice
Models:
class AutoVO - import_href , vin, sold
class SalesPerson - sales_person, employee_num
class Customer - customer_name, address, phone_number
class SaleRecord - price, automobile, sales_person, customer


Views:
api_list_salespeople - GET, POST
api_show_salesperson - GET
api_list_customers - GET, POST
api_list_salerecord - GET, POST


Within the API of the Sales Department, the models for SalesRecord and SalesPerson are entities within the bounded context of the Sales Department. Both are entities because both have have life-cycle and state. The Value Object used by the AutoVO is used as the key identifier/tie between the objects/models needed to be accessed in the Sales API. We can consider the enitr 'problem domain' as 'The Dealership' with three bounded contexts: Sales; Inventory; Services. The inventory is the base aggregate that the other two API's will access in order to communicate between one another, via the VIN property of the value object.
