import React from "react";



class CreateTechForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
                name: '',
                employeeID: '',
                hasSubmit: false,
        }

        this.handleChange = this.handleChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }




    async handleSubmit(event){
        event.preventDefault()
        const data = {...this.state}
        const techsURL = 'http://localhost:8080/api/technicians/'
        const fetchs = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
    }
    const response = await fetch(techsURL, fetchs)
    if (response.ok){
        const CreateTechForm  = await response.json()
        this.setState(
            {
            name: '',
            employeeID: '',
        }
        )

    }

    }

handleChange = (event) => {
    const value = event.target.value;
    const name = event.target.name;
    this.setState({[name]: value })
}

render() {
    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
                    <h1>Onboard a new Technician</h1>
                    <form onSubmit={this.handleSubmit} id="create-tech-form" >
                        <div className="form-floating mb3">
                           <input onChange={this.handleChange} value= {this.state.name} required name ="name" placeholder="name"  type= "text"  className="form-control" />
                           <label htmlFor="name">Technician</label>
                        </div>
                        <div className="form-floating mb3">
                           <input onChange={this.handleChange} value={this.state.employeeID} required name = "employeeID" placeholder="employeeID"  type="number"  className="form-control" />
                           <label htmlFor="employeeID">Employee ID</label>
                        </div>
                        <button className="btn btn-primary">Hire Technician</button>
                    </form>
        </div>
        </div>
        </div>
    )
}
}

export default CreateTechForm
