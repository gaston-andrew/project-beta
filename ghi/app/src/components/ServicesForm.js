import React from "react";

class ServicesForm extends React.Component {
    constructor() {
        super()
        this.state = {
            userInput: {
                owner: '',
                date: '',
                VIN: '',
                tech: '',
                service: '',
                isCompleted: false,
            },
            techs: [],
            hasSubmit: false,
        }
        this.handleChange = this.handleChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    async componentDidMount() {
        const techURL = 'http://localhost:8080/api/technicians'
        const response = await fetch(techURL)
        if (response.ok) {
            const techData = await response.json()
            this.setState({ techs: techData.technicians })
        }

    }

    handleChange = async (e) => {
        const value = e.target.value
        const name = e.target.name
        this.setstate({
            userInput: {
                ...this.state.userInput,
                [name]: value
            }
        })
    }

    handleSubmit = async (e) => {
        e.preventDefault()
        const data = { ...this.state }
        const serviceURL = 'http://localhost:8080/api/services'
        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        }
        const response = await fetch(serviceURL, fetchConfig)
        if (response.ok) {
            this.setState(
                {
                    owner: '',
                    date: '',
                    tech: '',
                    VIN: '',
                    service: '',
                }
            )
        }
    }

    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Book a New Service Appointment</h1>
                        <form onSubmit={this.handleSubmit} id="create-serviceapt-form">
                            <div className="form-floating mb-3">
                                <input onChange={this.handleChange} value={this.state.owner} placeholder="Name" required type="text" name="owner" id="owner" className="form-control" />
                                <label htmlFor="owner">Your Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleChange} value={this.state.VIN} placeholder="VIN" required type="text" name="VIN" id="vin" className="form-control" />
                                <label htmlFor="vin_customer">Car VIN</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleChange} value={this.state.date} placeholder="Date" required type="date" name="date" id="date" className="form-control" />
                                <label htmlFor="date">Choose a date</label>
                            </div>
                            <div className="mb-3">
                                <select onChange={this.handleChange} value={this.state.techs} required name="tech" id="tech" className="form-select">
                                    <option value="">Choose a technician</option>
                                    {this.state.techs.map(technician => {
                                        return (
                                            <option key={technician.employee_number} value={technician.name}>{technician.name}</option>
                                        )
                                    })}
                                </select>
                            </div>
                            <div className="mb-3">
                                <label htmlFor="reason">Work Order</label>
                                <textarea onChange={this.handleChange} value={this.state.service} className="form-control" name="service" id="service" rows="2"></textarea>
                            </div>
                            <button className="btn btn-success">Make an appointment</button>

                        </form>

                    </div>
                </div>
            </div>


        )
    }
}


export default ServicesForm
