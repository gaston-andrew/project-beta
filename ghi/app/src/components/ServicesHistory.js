import React from "react"




class ServicesHistory extends React.Component{
   constructor(){
   super()
    this.state = {
        appointments:[],
        userInput: '',
        hasSubmit: false
    }
    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
   }
    handleChange = (e) =>{
        const value = e.target.value
        this.setState({ userInput: value})
    }

    handleSubmit = async (e) => {
        e.preventDefault()

        const appointmentsURL = 'http://localhost:8080/api/services/'
        const response = await fetch(appointmentsURL)
        if (response.ok) {
            let results = await response.json()
            console.log(results)
            let terminus = []
            for (let appointment of results){
                if (appointment.VIN === this.state.userInput){
                    terminus.push(appointment)
                }
            }
            this.setState({
                appointments: terminus,
                hasSubmit: true,
            })
        }
    }


render() {
    let defaultClasses = "table table-striped d-none"
    if (this.state.hasSubmit){
        defaultClasses = 'table table-striped'
    }

return (
    <div className="container">
      <h1>Services History</h1>
            <div className= "input group mb-3">
            <input onChange={this.handleChange} required type="text" name="input" id="input" className="'form-control" placeholder="Find Vin" aria-describedby="button-addon2"/>
            <button onClick={this.handleSubmit} className="btn btn-success" type="button" id="button-search">FIND IT</button>
      </div>
      <div className="row">
        <table className={defaultClasses}>
            <thead>
                <tr>
                    <th>VIN</th>
                    <th>Customer</th>
                    <th>Date</th>
                    <th>Technician/s</th>
                    <th>Reason for Service</th>
                </tr>
            </thead>
            <tbody>
                {this.state.appointments.map(appointment => {
                    return(
                        <tr key={ appointment.id }>
                            <td>{ appointment.VIN }</td>
                            <td>{ appointment.owner }</td>
                            <td>{ appointment.date }</td>
                            <td>{ appointment.tech.name }</td>
                            <td>{ appointment.service }</td>
                        </tr>
                    )})}
            </tbody>
        </table>
        </div>
   </div>
    )
}
}


export default ServicesHistory
