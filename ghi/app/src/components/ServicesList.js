import React from "react";

class ServicesList extends React.Component{
    constructor() {
        super()
        this.state= {
        appointments: [],
        updated: false,
        }
        this.handleUpdate = this.handleUpdate.bind(this)
        this.handleDelete = this.handleDelete.bind(this)

    }

        async componentDidMount() {
            const appointmentsURL = `http://localhost:8080/api/services`;
            const response = await fetch(appointmentsURL)
            if(response.ok){
                let appointmentData = await response.json()
                this.setState({
                    appointments: appointmentData.appointments,
                    updated: false,
                })
            }

        }


    hanleUpdate = async (e) => {
        e.preventDefault()
        const id = e.target.value

        const appointmentsURL = `http://localhost:8080/api/services/${id}`
        const fethcOptions= {
            method: 'put',
            body: JSON.stringify({status: true }),
            headers: {
                'Content-Type': 'application.json'
                }
            }

        const response = await fetch(appointmentsURL, fethcOptions)
        if (response.ok){
            this.setState({
                updated: true,
                appointments: [],
            })
            this.componentDidMount()
            }
        }

    handleDelete = async (e) => {
        e.preventDefault()
        const id = e.target.value

        const appointmentsURL = `http://localhost:8080/api/services/${id}`
        const fethcOptions = {
            method: 'put',
            body: JSON.stringify({ status: true}),
            headers: {
                'Content-Type': 'application/json'
            },
        }
        const response = await fetch(appointmentsURL, fetchOptions)
        if(response.ok){
            this.setState({
                updated: true,
                appointments: [],
            })
            this.componentDidMount();
        }

    }
render () {

    return (
            <div className="container">
                <h2>Service Appointments</h2>
                <div className="row">
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>VIP Status</th>
                            <th>VIN #</th>
                            <th>Customer Name</th>
                            <th>Date</th>
                            <th>Technician</th>
                            <th>Reason for Service</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.appointments.filter(function (appointment) { return apppointment.status === false }).map(appointment => {
                        return (
                            <tr key={ appointment.id }>
                                <td>{ appointment.vip ? <div><img className="thumbnail" src= 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTs_Ut9yN0QvD8p5-5ziju8mCw6657kFG9ovg&usqp=CAU'/></div> : ''}</td>
                                <td>{ appointment.vin }</td>
                                <td>{ appointment.owner }</td>
                                <td>{ appointment.date }</td>
                                <td>{ appointment.tech }</td>
                                <td>{ appointment.service }</td>
                                <td><button type="button" value={apppointment.id} onClick={this.hanleUpdate} className="btn btn-success">Finished</button></td>
                                <td><button type="button" value={appointment.id} onClick={this.handleDelete} className="btn btn-danger">Cancel</button></td>
                            </tr>
                        )
                        })}
                    </tbody>
                </table>
                </div>

            </div>

    )
}
}
export default ServicesList
