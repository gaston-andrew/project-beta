import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ManufacturerList from './InventoryComponents/ManufacturorList';
import VehicleModelList from './InventoryComponents/VehichleModelsList';
import AutoList from './InventoryComponents/AutomobileList';
import ManufacturerForm from './InventoryComponents/ManufacturerForm';
import VehicleForm from './InventoryComponents/VehicleModelForm'
import AutoForm from './InventoryComponents/AutoForm';
import AddPotentialCustomer from './SalesComponents/CustomerForm';
import AddSalesPerson from './SalesComponents/SalesPersonForm';
import SalesHistory from './SalesComponents/SalesHistory';
import SalesList from './SalesComponents/SalesList';
import AddNewSale from './SalesComponents/NewSale'
import CreateTechForm from "./components/TechnicianForm";
import ServicesList from "./components/ServicesList";
import ServicesForm from './components/ServicesForm';
import ServicesHistory from './components/ServicesHistory';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path= "/" element= {<MainPage/>} />
          <Route path="/newsale" element={<AddNewSale/>} />
          <Route path= "/newsalesperson" element= {<AddSalesPerson/>} />
          <Route path= "/saleshistory" element= {<SalesHistory/>} />
          <Route path= "/newcustomer" element= {<AddPotentialCustomer/>} />
          <Route path= "/saleslist" element= {<SalesList/>} />
          <Route path= "/manufacturerlist" element= {<ManufacturerList/>} />
          <Route path= "/vehiclelist" element= {<VehicleModelList/>} />
          <Route path= "/autolist" element= {<AutoList/>} />
          <Route path= "/manufacturerform" element= {<ManufacturerForm/>} />
          <Route path= "/vehicleform" element= {<VehicleForm/>} />
          <Route path= "/autoform" element= {<AutoForm/>} />
          <Route path="/technician"/>
          <Route path="create/" element={<CreateTechForm/>}/>
           <Route path="/services"/>
          <Route path="create" element={<ServicesForm />} />
          <Route path="list" element={<ServicesList />} />
          <Route path="history" element={<ServicesHistory />} />

        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
