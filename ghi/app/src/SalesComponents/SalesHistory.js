import React from "react";

class SalesHistory extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            salespeople:[],
            salesperson: "",
            sales: [],
            salesfilter:[],
        };
        this.handleChange = this.handleChange.bind(this);
    }
    handleChange(event) {
        const value= event.target.value;
        this.setState({salesperson:value}, ()=>{this.filtering() })
    }

    filtering(){
        const results= this.state.sales.filter( sale=> sale.sales_person.employee_num=== Number(this.state.salesperson))
        this.setState({ salesfilter: results})
    }

    async componentDidMount(){
        const saleUrl = "http://localhost:8090/api/salerecord/"
        const salespersonUrl = "http://localhost:8090/api/salespeople/"

        const saleResponse = await fetch(saleUrl)
        const salesPersonResponse= await fetch(salespersonUrl)

        if(saleResponse.ok && salesPersonResponse.ok){
            const salepersondata = await salesPersonResponse.json()
            const saledata = await saleResponse.json()

            this.setState({salespeople: salepersondata.salespeople})
            this.setState({sales: saledata.salerecord, salesfilter: saledata.salerecord})
        }}
    render(){
        return(
            <div className="container">
            <h1> Sales Person History</h1>
            <div className="mb-3">
                <select onChange={this.handleChange} value= { this.state.salesperson} required name="salesperson" className="form-select">
                    <option value= ""> Choose Sales Person</option>
                    {this.state.salespeople.map(salesperson => {
                        return(
                            <option key={salesperson.employee_num} value= {salesperson.employee_num}> {salesperson.name}</option>
                        )
                    })}
                </select>

            </div>
            <table className= "table table-striped">
              <thead>
                <tr>
                  <th> Salesperson </th>
                  <th> Customer</th>
                  <th> VIN</th>
                  <th> Sale price</th>

                </tr>
              </thead>
              <tbody>
                {this.state.sales.map(sale => {
                  return (
                    <tr key={sale.id}>
                      <td>{sale.sales_person.sales_person} </td>
                      <td>{sale.customer.customer_name}</td>
                      <td>{sale.automobile.vin}</td>
                      <td>{sale.price}</td>
                    </tr>
                  )
                })}
              </tbody>
            </table>
            </div>
        )
    }
}
export default SalesHistory;
