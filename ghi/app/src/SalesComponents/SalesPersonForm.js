import React from 'react'


class AddSalesPerson extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            sales_person: "",
            employee_num: "",
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);

    }
    handleChange=(event)=> {
        const value = event.target.value;
        const name = event.target.name;
        this.setState({ [name]: value})
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state}
        const url = 'http://localhost:8090/api/salespeople/';
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
            'Content-Type': 'application/json',
            },
        }

        const response = await fetch(url, fetchConfig);
        if(response.ok){
                const AddSalesPerson = await response.json();

           this.setState({
                sales_person: "",
                employee_num: '',
              });
        }

    }
        // async componentDidMount() {
        //     const url= "http://localhost:8090/api/salespeople/"

        //     const response = await fetch(url)

        //     if (response.ok) {
        //         const data = await response.json();

        //         this.setState(
        //             {
        //                 salespersons: data.salespersons,
        //             }
        //         )
        //     };
        // }



    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Add A Salesperson</h1>
                        <form onSubmit={this.handleSubmit} id= "create-salesperson-form">
                            <div className= "form-floating mb-3">
                                <input onChange={this.handleChange} value= {this.state.employee_num} required name= "employee_num" placeholder= "employee_num" type= "text" className= "form-control" />
                                <label htmlFor='=employee_num'> Employee Number</label>
                            </div>
                            <div className= "form-floating mb-3">
                                <input onChange= {this.handleChange} value={this.state.sales_person} required name= "sales_person" placeholder="sales_person" type="text" className='form-control'></input>
                                <label htmlFor="=salesperson"> Salesperson Name</label>
                            </div>
                            <button className="btn btn-primary">Add</button>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}


export default AddSalesPerson;
