import React from "react";

class AddPotentialCustomer extends React.Component{
    constructor(props){
    super(props)
    this.state = {
    customer_name: "",
    address: "",
    phone_number:"",
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
    }
    handleChange=(event) =>{
      const value = event.target.value;
      const name = event.target.name;
      this.setState({[name]: value})
    }

    async handleSubmit(event) {
      event.preventDefault();
      const data = {...this.state}

      const url = 'http://localhost:8090/api/customers/'
      const fetchConfig= {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
          'Content-Type': 'application/json',
          }
        }

        const response = await fetch(url, fetchConfig);
        if(response.ok){
                const addCustomer = await response.json();

           this.setState({
                customer_name: "",
                address: '',
                phone_number: '',
              });
        }
    }
    render(){
    return(
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add Potential Customer</h1>
            <form onSubmit={this.handleSubmit} id="create-customer-form">
              <div className="form-floating mb-3">
                <input onChange={this.handleChange} value={this.state.customer_name} required name="customer_name" placeholder="customer_name"  type="text"  className="form-control" />
                <label htmlFor="name">Customer Name</label>
              </div>
              <div className="form-floating mb-3">
                <label htmlFor="name">Address </label>
                <input onChange={this.handleChange}  value= {this.state.address} required name="address" placeholder="Address" type="text"  className="form-control"></input>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleChange} value={this.state.phone_number} required name= "phone_number" placeholder="Phone Number" type="number"  className="form-control" />
                <label htmlFor="name"> Phone Number</label>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    )

}
}
export default AddPotentialCustomer;
