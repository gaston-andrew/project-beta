 import React from "react";

class AddNewSale extends React.Component{
constructor(props) {
    super(props)
    this.state={
    automobile:'',
    autos:[],
    sales_person: '',
    salespeople:[],
    customer: '',
    customers:[],
    price: '',

}
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleChange.bind(this);
}


handleChange(event) {
    const value = event.target.value;
    const name = event.target.name;
    this.setState({[name]: value})
}

async handleSubmit(event) {
    event.preventDefault()
    const data = {...this.state}
    delete data.autos
    delete data.salespeople
    delete data.customers


    const vin = data.automobile
    const autosUrl= `http://localhost:8100/api/automobiles/${vin}/`
    const autoFetch = {
        method: "PUT",
        body: JSON.stringify({
            "sold": true
        }),
        headers: {
            'Content-Type': 'application.json'
        }
    }

    const saleUrl = 'http://localhost:8090/api/salerecord/'
        const saleFetch = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        }

    const response = await fetch(autosUrl, autoFetch)
        const response2 = await fetch(saleUrl, saleFetch)
    if (response.ok && response2.ok) {
        this.setState({
            automobile: "",
            sales_person: "",
            customer: "",
            price: "",
        })
    }

}


async componentDidMount() {
        const autoUrl = 'http://localhost:8100/api/automobiles/';
        const salesUrl = 'http://localhost:8090/api/salespeople/';
        const customerUrl = 'http://localhost:8090/api/customers/';

        const autoResponse = await fetch(autoUrl);
        const salesResponse = await fetch(salesUrl);
        const customerResponse = await fetch(customerUrl);


        if (autoResponse.ok && salesResponse.ok && customerResponse.ok) {



            const autoData = await autoResponse.json();
            const customerData = await customerResponse.json();
            const salesData = await salesResponse.json();
            let filterAutos=  await autoData.autos.filter(automobile => automobile.sold === false)
            this.setState({
                autos: filterAutos,
                salespeople: salesData.salespeople,
                customers: customerData.customers,

            });
        }
}


render() {
    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Record a new sale</h1>
                    <form onSubmit={this.handleSubmit} id="create-sale-form">
                        <div className="mb-3">
                            <select onChange={this.handleChange} value={this.state.automobile} required name="automobile" className="form-select">
                                <option value="">Choose an automobile</option>
                                {this.state.autos.map(automobile => {
                                    return (
                                        <option key={automobile.id} value={automobile.vin}>
                                            {automobile.vin}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <div className="mb-3">
                            <select onChange={this.handleChange} value={this.state.sales_person} required name="salesperson" className="form-select">
                                <option value="">Choose a salesperson</option>
                                {this.state.salespeople.map(salesperson => {
                                    return (
                                        <option key={salesperson.id} value={salesperson.employee_num}>
                                            {salesperson.sales_person}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <div className="mb-3">
                            <select onChange={this.handleChange} value={this.state.customer} required name="customer_name" className="form-select">
                                <option value="">Choose a customer</option>
                                {this.state.customers.map(customer => {
                                    return (
                                        <option key={customer.id} value={customer.phone_number}>
                                            {customer.customer_name}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={this.handleChange} value={this.state.price} required name="price" placeholder="Price" type="text" className="form-control"/>
                            <label htmlFor="Price">Price</label>
                        </div>
                        <button className="btn btn-primary">Add</button>
                    </form>
                </div>
            </div>
        </div>
    );

    }
}

export default AddNewSale;
