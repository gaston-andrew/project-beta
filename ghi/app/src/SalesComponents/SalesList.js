import React from 'react';


class SalesList extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            salerecord:[],
        };
    }
    async componentDidMount(){
        const url = 'http://localhost:8090/api/salerecord/'

        const response = await fetch(url);

        if(response.ok){
          const data= await response.json();
          this.setState({salerecord: data.salerecord})
        }
    }

    render(){
        return (
      <div className='container'>
           <table className="table table-striped">
          <thead>
            <tr>
              <th>Automobile</th>
              <th>Sales Person</th>
              <th> Customer</th>
              <th> Price</th>
            </tr>
          </thead>
          <tbody>
            {this.state.salerecord.map(sale => {
              return (
                <tr key={sale.id}>
                  <td>{ sale.automobile.vin }</td>
                  <td>{sale.sales_person.employee_num}</td>
                  <td>{sale.customer.customer_name}</td>
                  <td>{sale.price}</td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
          );
        }

    }
export default SalesList
