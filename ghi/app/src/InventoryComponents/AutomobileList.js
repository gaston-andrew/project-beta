import React from "react";

class AutoList extends React.Component {
    constructor(props){
        super(props)
        this.state={
          autos: [],
        };
      }
      async componentDidMount(){
        const url = "http://localhost:8100/api/automobiles/"
        const response = await fetch (url)

        if(response.ok){
          const data= await response.json()
          this.setState({autos: data.autos});
        }
    }

  render() {
    return(
      <div className="container">
      <h1> Models List</h1>
      <table className= "table table-striped">
        <thead>
          <tr>
            <th> Vehicle Model Name </th>
            <th> Manufacturer</th>
            <th>VIN</th>
            <th>Color</th>
            <th>Year</th>
          </tr>
        </thead>
        <tbody>
          {this.state.autos.map(auto => {
            return (
              <tr key={auto.id}>
                <td>
                  {auto.name}
                </td>
                <td>
                    {auto.vin}
                </td>
                <td>
                    {auto.year}
                </td>
                <td>
                    {auto.model.name}
                </td>
                <td>
                    {auto.model.manufacturer.name}
                </td>
                <td>

                </td>
              </tr>
            )
          })}
        </tbody>
      </table>
      </div>

    )
  }
}

export default AutoList;
