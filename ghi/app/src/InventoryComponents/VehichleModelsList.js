import React from "react";

class VehicleModelList extends React.Component {
    constructor(props){
        super(props)
        this.state= {
          vehiclemodel: [],
        };
      }
      async componentDidMount(){
        const url = "http://localhost:8100/api/models/"
        const response = await fetch (url)

        if(response.ok){
          const data= await response.json()
          this.setState({vehiclemodels: data.vehiclemodels});
        }
    }

  render() {
    return(
      <div className="container">
      <h1> Models List</h1>
      <table className= "table table-striped">
        <thead>
          <tr>
            <th> Vehicle Model Name </th>
            <th> Manufacturer</th>

          </tr>
        </thead>
        <tbody>
          {this.state.vehiclemodels.map(vehiclemodel => {
            return (
              <tr key={vehiclemodel.id}>
                <td>
                  {vehiclemodel.name}
                </td>
                <td>
                    {vehiclemodel.manufacturer.name}
                </td>
                <td>

                </td>
              </tr>
            )
          })}
        </tbody>
      </table>
      </div>

    )
  }
}

export default VehicleModelList;
