from django.db import models
# from models import ServiceAppointment
# Create your models here.


class AutomobileVO(models.Model):
    VIN = models.CharField(max_length=17, unique=True)



class Customer(models.Model):
    name = models.CharField(max_length=100)
    vehicles = models.ForeignKey(AutomobileVO, on_delete=models.PROTECT)



class Technician(models.Model):
    name = models.CharField(max_length=100)
    employeeID = models.PositiveIntegerField()


class ServiceAppointment(models.Model):
    VIN  = models.CharField(max_length=17)
    owner = models.CharField(max_length=100,null=True)
    date = models.DateTimeField(auto_now_add=False, blank=True)
    tech = models.ForeignKey(Technician, on_delete=models.PROTECT)
    service = models.CharField(max_length=200)
    isCompleted = models.BooleanField(default=False)
