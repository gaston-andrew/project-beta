from django.urls import path
from .views import api_tech_list, api_service_details, api_service_list
from django.contrib import admin


urlpatterns = [

    path('technicians/', api_tech_list, name="tech_list"),
    path('services/<int:pk>/', api_service_details, name="service_detail"),
    path('services/', api_service_list, name="service_list")
    ]
