from django.contrib import admin
from .models import ServiceAppointment, Technician, AutomobileVO, Customer
# Register your models here.

admin.site.register(Technician)
admin.site.register(ServiceAppointment)
admin.site.register(AutomobileVO)
admin.site.register(Customer)
