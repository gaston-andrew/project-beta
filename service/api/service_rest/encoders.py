from common.json import ModelEncoder
# from inventory.api.inventory_rest.models import Automobile
from .models import  AutomobileVO, ServiceAppointment, Technician



class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "VIN"
        ]


class TechnicianDetailEncoder(ModelEncoder):
    model = Technician
    properties = [
        "name",
        "appointment",
        "service",
        ]

    def get_extra_data(self, o):
        return {"VIN": o.inventory.VIN}

class TechnicianListEncoder(ModelEncoder):
    model = Technician
    properties = [
        "name",
         "employeeID",
         ]

    # def get_extra_data(self, o):
    #      return {
    #          "active appointments": o.serviceappointment.appointment,
    #          "service": o.serviceappointment.service
    #      }



class ServiceListEncoder(ModelEncoder):
    model = ServiceAppointment
    properties = [
        "id",
        "owner",
        "date",
        "service",
        "isCompleted",
        "VIN",
        "tech",
    ]
    encoders = {
        "tech": TechnicianListEncoder(),
    }



# class ServiceDetailEncoder(ModelEncoder):
#     model = ServiceAppointment
#     properties = [
#         "owner",
#         "appointment",
#         "tech",
#         "service",
#         "isCompleted",
#     ]
#     encoders = {
#         "vin": AutomobileVOEncoder(),
#     }
