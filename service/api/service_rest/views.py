from django.shortcuts import render
import json
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
from .models import Technician, ServiceAppointment, AutomobileVO
from .encoders import (AutomobileVOEncoder,
                        TechnicianListEncoder,
                         ServiceListEncoder,
                         TechnicianDetailEncoder
                         )

# Create your views here.


@require_http_methods(["GET", "POST"])
def api_tech_list(request):
    if request.method == "GET":
        techs = Technician.objects.all()
        return JsonResponse(
            {"Technicians": techs},
             encoder=TechnicianListEncoder,
             )
    else: # the request must be a POST
        content = json.loads(request.body)

        try:
            techs = Technician.objects.create(**content)
            return JsonResponse(
            techs,
            encoder= TechnicianListEncoder,
            safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Technician cannot work here"},
            )
            response.status_code=400
            return response


@require_http_methods(["GET", "POST"])
def api_service_list(request):

    if request.method == "GET":
        services = ServiceAppointment.objects.all()
        return JsonResponse(
            services,
            encoder=ServiceListEncoder,
            safe=False
            )

    else:

        try:
            content = json.loads(request.body)
            tech_employeeID = content["tech"]
            tech = Technician.objects.get(employeeID = tech_employeeID)
            content["tech"] = tech


        except:
            response = JsonResponse(
                {"message": "Service appointment not found/cannot be made"}
                )
            response.status_code = 400
            return response

        service = ServiceAppointment.objects.create(**content)

        return JsonResponse(
                service,
                encoder=ServiceListEncoder,
                safe=False
                )



@require_http_methods(["GET", "DELETE","PUT"])
def api_service_details(request, pk):
    if request.method == "GET":
        try:
            appointment = ServiceAppointment.objects.get(id=pk)
            return JsonResponse(
                appointment, encoder=ServiceListEncoder,
                safe=False
            )
        except ServiceAppointment.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            appointment = ServiceAppointment.objects.filter(id=pk)
            appointment.delete()
            return JsonResponse(
                {"deleted": "Appointment has been deleted"}
                )
        except ServiceAppointment.DoesNotExist:
            return JsonResponse(
                {"message":  "Does not Exist"}
                )
    else: #PUT
        content = json.loads(request.body)
        try:
            if "isCompleted" in content:
                currentAppointment = ServiceAppointment.objects.get(id=pk)
                currentAppointment["isCompleted"] = True
                currentAppointment.save()
        except ServiceAppointment.DoesNotExist:
            return JsonResponse(
                {"message" : "Service does not exist"}
            )
