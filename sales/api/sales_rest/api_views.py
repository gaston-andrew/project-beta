
from .models import SaleRecord, AutoVO, Customer, SalesPerson
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from .encoders import AutoVODetailEncoder , SalespersonDetailEncoder,SalespersonListEncoder, CustomerDetailEncoder, CustomerListEncoder, SaleRecordDetailEncoder, SaleRecordListEncoder

#  MOVED ENCODERS TO SEPERATE FILE (encoders.py)

# class AutoVODetailEncoder(ModelEncoder):
#     model = AutoVO
#     properties = ["import_href", "vin"]

# class SalespersonListEncoder(ModelEncoder):
#     model = SalesPerson
#     properties = ["id", "sales_person", "employee_num"]

# class SalespersonDetailEncoder(ModelEncoder):
#     model = SalesPerson
#     properties = ["id", "sales_person", "employee_num"]

# class CustomerListEncoder(ModelEncoder):
#     model = Customer
#     properties = ["id", "customer_name", "address", "phone_number"]

# class CustomerDetailEncoder(ModelEncoder):
#     model = Customer
#     properties = ["id", "customer_name", "address", "phone_number"]

# class salerecordListEncoder(ModelEncoder):
#     model = salerecord
#     properties = [
#         "id",
#         "price",
#         "automobile",
#         "salesperson",
#         "customer"
#         ]

#     encoders = {
#         "automobile": AutoVODetailEncoder(),
#         "salesperson": SalespersonListEncoder(),
#         "customer": CustomerListEncoder(),
#     }

# class salerecordDetailEncoder(ModelEncoder):
#     model = salerecord
#     properties = [
#         "id",
#         "price",
#         "automobile",
#         "salesperson",
#         "customer"
#         ]

#     encoders = {
#         "automobile": AutoVODetailEncoder(),
#         "salesperson": SalespersonListEncoder(),
#         "customer": CustomerListEncoder(),
#     }

@require_http_methods(["GET", "POST","PUT"])
def api_list_salespeople(request):
    if request.method == "GET":
        salespeople = SalesPerson.objects.all()
        return JsonResponse(
            {"salespeople": salespeople},
            encoder = SalespersonListEncoder,
        )
    else:
        content = json.loads(request.body)
        salesperson = SalesPerson.objects.create(**content)
        return JsonResponse(
            salesperson,
            encoder = SalespersonDetailEncoder,
            safe=False,
        )

@require_http_methods(["GET"])
def api_show_salesperson(request, pk):
    if request.method == "GET":
        salesperson = SalesPerson.objects.get(id=pk)
        return JsonResponse(
            salesperson,
            encoder=SalespersonDetailEncoder,
            safe=False,
        )

@require_http_methods(["GET", "POST","PUT"])
def api_list_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder = CustomerListEncoder
        )
    else:
        content = json.loads(request.body)
        customer = Customer.objects.create(**content)
        return JsonResponse(
            customer,
            encoder = CustomerDetailEncoder,
            safe=False,
        )





@require_http_methods(["GET", "POST","PUT"])
def api_list_salerecord(request):
    if request.method == "GET":
        salerecord = SaleRecord.objects.all()
        return JsonResponse(
            {"salerecord": salerecord},
            encoder = SaleRecordListEncoder
        )
    else:
        content = json.loads(request.body)
        try:
            automobile = AutoVO.objects.get(vin=content["automobile"])
            content["automobile"] = automobile
        except AutoVO.DoesNotExist:
            return JsonResponse(
                {"message": "VIN provided is Invalid"},
                status=400,
            )
        try:
            salesperson = content["sales_person"]
            content["sales_person"] = SalesPerson.objects.get(sales_person=salesperson)

        except SalesPerson.DoesNotExist:
            return JsonResponse(
                {"message": "Salesperson provided is invalid"},
                status = 400,
            )
        try:
            customer = Customer.objects.get(customer_name=content["customer"])
            content["customer"] = customer
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "customer name not found"}
            )

        salerecord = SaleRecord.objects.create(**content)
        return JsonResponse(
            salerecord,
            encoder = SaleRecordDetailEncoder,
            safe=False,
        )
