from django.urls import path
from .api_views import (api_list_customers, api_list_salespeople,
    api_show_salesperson,api_list_salerecord
    )

urlpatterns = [
    path("customers/", api_list_customers, name="api_list_customers"),
    path("salespeople/", api_list_salespeople, name="api_list_salespeople"),
    path("salespeople/<int:pk>/", api_show_salesperson, name="api_show_salesperson"),
    path("salerecord/", api_list_salerecord, name= "api_list_salerecord" )

]
