from .models import SaleRecord, AutoVO, Customer, SalesPerson
from common.json import ModelEncoder
import json
from django.http import JsonResponse


class AutoVODetailEncoder(ModelEncoder):
    model = AutoVO
    properties = ["import_href", "vin"]

class SalespersonListEncoder(ModelEncoder):
    model = SalesPerson
    properties = ["id", "sales_person", "employee_num"]

class SalespersonDetailEncoder(ModelEncoder):
    model = SalesPerson
    properties = ["id", "sales_person", "employee_num"]

class CustomerListEncoder(ModelEncoder):
    model = Customer
    properties = ["id", "customer_name", "address", "phone_number"]

class CustomerDetailEncoder(ModelEncoder):
    model = Customer
    properties = ["id", "customer_name", "address", "phone_number"]

class SaleRecordListEncoder(ModelEncoder):
    model = SaleRecord
    properties = [
        "id",
        "price",
        "automobile",
        "sales_person",
        "customer"
        ]

    encoders = {
        "automobile": AutoVODetailEncoder(),
        "sales_person": SalespersonListEncoder(),
        "customer": CustomerListEncoder(),
    }

class SaleRecordDetailEncoder(ModelEncoder):
    model = SaleRecord
    properties = [
        "id",
        "price",
        "automobile",
        "sales_person",
        "customer"
        ]

    encoders = {
        "automobile": AutoVODetailEncoder(),
        "sales_person": SalespersonListEncoder(),
        "customer": CustomerListEncoder(),
    }
