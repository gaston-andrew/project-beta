from django.db import models

class AutoVO(models.Model):
    import_href = models.CharField(max_length=200 , unique=True)
    vin= models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)

    def __str__(self):
        return self.vin

class SalesPerson(models.Model):
    sales_person= models.CharField(max_length=100)
    employee_num= models.PositiveSmallIntegerField(unique=True)

    def __str__(self):
        return self.sales_person

class Customer(models.Model):
    customer_name = models.CharField(max_length =100)
    address = models.CharField(max_length= 100)
    phone_number = models.CharField(max_length=20)

    def __str__(self):
        return self.customer_name

class SaleRecord(models.Model):
    price= models.FloatField(null=True)
    automobile= models.ForeignKey( AutoVO, related_name= "salerecords",
    on_delete= models.PROTECT
    )

    sales_person = models.ForeignKey(
        SalesPerson,
        related_name="salerecords",
        on_delete=models.PROTECT
    )
    customer = models.ForeignKey(
        Customer,
        related_name="salerecords",
        on_delete=models.PROTECT
    )
